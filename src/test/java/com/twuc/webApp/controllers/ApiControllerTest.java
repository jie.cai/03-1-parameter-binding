package com.twuc.webApp.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Contract;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ApiControllerTest {

    @Autowired
    MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void should_get_user_by_path_variable_id() throws Exception {
        mockMvc.perform(get("/api/users/123"))
                .andExpect(content().string("id 123"));
    }

    @Test
    void should_get_user_by_path_variable_id_use_int() throws Exception {
        mockMvc.perform(get("/api/users/123/int"))
                .andExpect(content().string("id 123"));
    }

    @Test
    void should_get_user_by_path_variable_id_bind_more() throws Exception {
        mockMvc.perform(get("/api/users/123/bind/more"))
                .andExpect(content().string("id 123 otherId 123"));
    }

    @Test
    void should_get_query_params() throws Exception {
        mockMvc.perform(
                get("/api/params")
                        .param("name", "tom")
                        .param("age", "12")
        )
                .andExpect(content().string("tom 12"));
    }

    @Test
    void should_get_query_params_with_default_value() throws Exception {
        mockMvc.perform(get("/api/params/default"))
                .andExpect(content().string("anonymous 0"));
    }

    @Test
    void should_get_query_params_with_collection() throws Exception {
        mockMvc.perform(
                get("/api/params/collection")
                        .param("collection", "a", "b", "c")
        )
                .andExpect(content().string("length 3"));
    }

    @Test
    void should_serialize_object_to_json() throws IOException {
        assertEquals(
                objectMapper.writeValueAsString(new Contract(233L)),
                "{\"id\":233}"
        );
        assertEquals(
                objectMapper.readValue("{\"id\":233}", Contract.class).getId(),
                Long.valueOf(233L)
        );
    }

    @Test
    void should_serialize_date() throws Exception {
        mockMvc.perform(
                post("/api/datetime")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\"dateTime\":\"2019-10-01T10:00:00Z\"}")
        )
                .andExpect(jsonPath("$.dateTime").value("2019-10-01T10:00:00Z"));
    }

    @Test
    void should_not_deserialize_date_with_simple_format() throws Exception {
        mockMvc.perform(
                post("/api/datetime")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\"dateTime\":\"2019-10-01\"}")
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_validate_post_body() throws Exception {
        mockMvc.perform(
                post("/api/person")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\"name\":\"Tom\",\"age\":12}")
        )
                .andExpect(content().string("Tom"));

        mockMvc.perform(
                post("/api/person")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\"name\":\"A\",\"age\":12}")
        )
                .andExpect(status().isBadRequest());

        mockMvc.perform(
                post("/api/person")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\"name\":\"Tom\",\"age\":-1}")
        )
                .andExpect(status().isBadRequest());
    }


    @Test
    void should_get_student_by_custom_response() throws Exception {
        mockMvc.perform(
                post("/api/student/custom")
                        .param("status", "233")
        )
                .andExpect(status().is(233));

        mockMvc.perform(
                post("/api/student/custom")
                        .param("status", "2019")
        )
                .andExpect(status().is(2019));
    }

    @Test
    void should_get_student_that_post_student() throws Exception {
        mockMvc.perform(
                post("/api/student/custom/json").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\n" +
                                "  \"name\":\"tom\",\n" +
                                "  \"age\": 12,\n" +
                                "  \"phone_number\": \"123\"\n" +
                                "}")
        )
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.name").value("tom"))
                .andExpect(jsonPath("$.age").value("12"))
                .andExpect(jsonPath("$.phone_number").value("123"));


    }

    @Test
    void should_get_student_that_post_student_without_age() throws Exception {
        mockMvc.perform(
                post("/api/student/custom/json").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\n" +
                                "  \"name\":\"tom\",\n" +
                                "  \"phone_number\": \"123\"\n" +
                                "}")
        )
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.name").value("tom"))
                .andExpect(jsonPath("$.age").doesNotExist())
                .andExpect(jsonPath("$.phone_number").value("123"));
    }
}
