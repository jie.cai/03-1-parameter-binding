package com.twuc.webApp;

import java.util.Date;

public class Contract {
    Long id;

    public Contract() {}

    public Contract(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
