package com.twuc.webApp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

public class DateTimeRequestMapping {
    @JsonProperty("dateTime")
    private ZonedDateTime localDateTime;

    public ZonedDateTime getLocalDateTime() {
        return localDateTime;
    }
}
