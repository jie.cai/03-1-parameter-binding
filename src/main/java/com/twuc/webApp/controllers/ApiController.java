package com.twuc.webApp.controllers;

import com.twuc.webApp.model.DateTimeRequestMapping;
import com.twuc.webApp.model.Person;
import com.twuc.webApp.model.Student;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

    // # 2.1

    @GetMapping("/users/{id}")
    public String getUserById(@PathVariable("id") Integer userId) {
        return "id " + userId;
    }

    @GetMapping("/users/{id}/int")
    public String getUserByIdUseInt(@PathVariable("id") int userId) {
        return "id " + userId;
    }

    @GetMapping("/users/{id}/bind/more")
    public String getUserByIdBindMore(
            @PathVariable("id") int userId,
            @PathVariable("id") int otherId) {
        return "id " + userId + " otherId " + otherId;
    }

    // # 2.2

    @GetMapping("/params")
    public String getQueryParams(
            @RequestParam String name,
            @RequestParam String age
    ) {
        return name + " " + age;
    }

    @GetMapping("/params/default")
    public String getQueryParamsWithDefaultValue(
            @RequestParam(defaultValue = "anonymous") String name,
            @RequestParam(defaultValue = "0") String age
    ) {
        return name + " " + age;
    }

    @GetMapping("/params/collection")
    public String getQueryParamsWithCollection(
            @RequestParam List<String> collection
    ) {
        return "length " + collection.size();
    }

    // # 2.3

    @PostMapping("/datetime")
    public DateTimeRequestMapping datetime(
            @RequestBody DateTimeRequestMapping dateTimeRequestMapping
    ) {
        return dateTimeRequestMapping;
    }

    // # 2.4

    @PostMapping("/person")
    public String person(
            @Valid
            @RequestBody Person person
    ) {
        return person.getName();
    }

    // more

    @PostMapping("/student/custom")
    public ResponseEntity<String> getCustomStudent(
            @RequestParam("status") int customStatusCode
    ) {
        return ResponseEntity.status(customStatusCode).build();
    }

    @PostMapping("/student/custom/json")
    public ResponseEntity<Student> getCustomStudentWithPostedData(
            @RequestBody Student student
    ) {
        return ResponseEntity.status(201).body(student);
    }
}
